#include "string_hash.h"


StringHash::Hash StringHash::hashOf(const std::string& str)
{
    return hashOf(str.c_str());
}


StringHash::Hash StringHash::hashOf(const char* str)
{
    if (!str || strcmp(str, "") == 0)
        return 0;

    Hash hash = 5381;
    while (*str)
    {
        hash = ((hash << 5) + hash) + *str;
        ++str;
    }
    return hash;
}
