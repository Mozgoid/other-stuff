#pragma once

#include "string_hash.h"
#include <unordered_map>

class StringId : protected StringHash
{
    typedef StringHash Base;

    struct StringInfo
    {
        std::string str;
        unsigned int count;
    };

public:
    explicit StringId(const std::string& str);
    explicit StringId(const char* str = "");
    StringId(const StringId& copy);

    ~StringId();

    StringId& operator=(const StringId& copy);

    const std::string& getStr() const
    {
        assert(s_stringTable.find(hash()) != end(s_stringTable));
        return s_stringTable[hash()].str;
    }

    unsigned int count() const
    {
        assert(s_stringTable.find(hash()) != end(s_stringTable));
        return s_stringTable[hash()].count;
    }

    using Base::hash;
    using Base::operator==;

    bool operator==(const StringHash& other) const
    {
        return hash() == other.hash();
    }

private:
    void decreaseCounter();
    void create(const char* str);

    static std::unordered_map<Hash, StringInfo> s_stringTable;
};