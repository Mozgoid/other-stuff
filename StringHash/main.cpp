// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#include "string_id.h"


int main()
{

    assert(StringId("").count() == 1);
    assert(StringId("") == "");
    assert(StringId(nullptr) == "");

    auto a1 = StringHash(nullptr);
    auto a2 = StringHash("");
    assert(a1 == a2);
    auto b1 = StringId("");
    assert(b1 == a1);
    auto b2 = StringId(nullptr);

    {
        auto b3 = StringId(nullptr);
        assert(b3.count() == 3);
    }
    assert(b2.count() == 2);

    auto b4 = StringId("4");
    auto b5 = StringId("5");
    {
        auto b55 = StringId("5");
        assert(b55.count() == 2);
    }
    assert(b5.count() == 1);
    auto b555 = StringId("5");
    assert(b555.count() == 2);
    assert(b555 == "5");


    /*
    auto sh = StringHash("some string");
    switch (sh.hash())
    {
    case staticHash("some string"):
    break;
    case staticHash("other string"):
    break;
    default:
    break;
    }
    */

    return 0;
}



