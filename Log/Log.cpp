#include "Log.h"

namespace Log
{
    static LogManager* g_instance = nullptr;

    LogManager* instance()
    {
        if (g_instance == nullptr)
        {
            g_instance = new LogManager();
            addStandardLoggers( StringId(DefaultLogName) );
        }
        return g_instance;
    }


    void destroyInstance()
    {
        //Assert(s_instance != nullptr);
        delete g_instance;
        g_instance = nullptr;
    }


    void addStandardLoggers(StringId name)
    {
        instance()->addLogger(std::make_shared<ConsoleLogger>(name));
    }


    void LogManager::log(StringId name, Level level, const String& message)
    {
        auto loggers = _loggers.equal_range(name.getStr());
        if (loggers.first == loggers.second)
        {
            if (name.getStr() == DefaultLogName)
            {
                //Assert(!"default log is not exist");
                return;
            }
            Log::warning("trying to write into not existing logger \"" + name.getStr() + "\" message: " + message);
            return;
        }

        for (auto it = loggers.first; it != loggers.second; ++it)
        {
            if (it->second->level() <= level)
            {
                it->second->log(message);
            }
        }
    }

    void LogManager::addLogger(ShPtr<Logger> logger)
    {
        auto& name = logger->name().getStr();
        _loggers.insert(make_pair(name, logger));

        Log::info( Log::createMessage("new logger added to log manager. currently have "
            , _loggers.count(name), "  loggers with name: ", name)  );
    }

    void debug(const String& message, StringId name /*= StringId(DefaultLogName)*/)
    {
        instance()->log(name, Debug, message);
    }

    void info(const String& message, StringId name /*= StringId(DefaultLogName)*/)
    {
        instance()->log(name, Info, message);
    }

    void warning(const String& message, StringId name /*= StringId(DefaultLogName)*/)
    {
        instance()->log(name, Warning, message);
    }

    void error(const String& message, StringId name /*= StringId(DefaultLogName)*/)
    {
        instance()->log(name, Error, message);
    }

}


