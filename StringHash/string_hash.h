#pragma once

#include <string>
#include <assert.h>

/*
constexpr is not available in vs2013 :(
with constexpr hashes can be used in 'case' (probably)
*/

#if _MSC_VER > 1800
constexpr const unsigned int hashStringRecursive(unsigned int hash, const char* str)
{
    return (!*str ? hash : hashStringRecursive(((hash << 5) + hash) + *str, str + 1));
}

constexpr const unsigned int staticHash(const char* str)
{
    return (!str ? 0 : hashStringRecursive(5381, str));
}
#endif

class StringHash
{
public:
    typedef unsigned int Hash;

    explicit StringHash(const std::string& str)
    {
        _hash = hashOf(str);
#if _DEBUG
        _debugString = str;
#endif
    }


    explicit StringHash(const char* str = "")
    {
        _hash = hashOf(str);
#if _DEBUG
        _debugString = str != nullptr ? str : "";
#endif
    }


    StringHash(const StringHash& _copy)
    {
        _hash = _copy._hash;
#if _DEBUG
        _debugString = _copy._debugString;
#endif
    }

    Hash hash() const
    {
        return _hash;
    }

    bool operator==(const std::string& other) const
    {
        return _hash == hashOf(other);
    }

    bool operator==(const char* other) const
    {
        return _hash == hashOf(other);
    }

    bool operator==(const StringHash& other) const
    {
        return _hash == other._hash;
    }

    static Hash hashOf(const std::string& str);
    static Hash hashOf(const char* str);

private:
#if _DEBUG
    std::string _debugString;
#endif
    Hash _hash;
};

