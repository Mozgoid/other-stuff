import math


def char_to_digit(char):
    digit = ord(char) - ord('0')
    assert(digit >= 0 and digit <= 9)
    return digit


def str_to_float(inputstr):
    assert(inputstr.find('-') in (-1, 0))
    assert(inputstr.count('-') in (0, 1))
    assert(inputstr.count('.') in (0, 1))

    result = 0.0
    digits_after_point = 0
    point_reached = False
    negative = False
    for c in inputstr:
        if c == '-':
            negative = True
            continue

        if c == '.':
            point_reached = True
            continue

        newdigit = char_to_digit(c)
        result = result * 10.0 + newdigit

        if point_reached:
            digits_after_point += 1

    result /= 10 ** digits_after_point

    if negative:
        result *= -1

    assert(math.isclose(result, float(inputstr)))
    return result


if __name__ == '__main__':
    print(str_to_float('1'))
    print(str_to_float('10'))
    print(str_to_float('1.1'))
    print(str_to_float('.1'))
    print(str_to_float('123123123.124124124124'))
    print(str_to_float('9999999999999999999999999999'))
    print(str_to_float('123123.2'))
    print(str_to_float('123123.0'))
    print(str_to_float('-123123.2'))
