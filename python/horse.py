import sys


def point2index(point):
    return ord(point[0].lower()) - 97, int(point[1])


def index2point(index):
    return chr(index[0] + 97) + str(index[1])


A, B = point2index(sys.argv[1]), point2index(sys.argv[2])
tablecells = set([(i, j) for i in range(8) for j in range(8)])
steps = [set((A,))]
prevstep = steps[0]
passed = set(steps[0])
diff = ((2, -1), (2, 1), (-2, -1), (-2, 1), (-1, -2), (-1, 2), (1, -2), (1, 2))

while B not in passed:
    nextstep = set([(x + dx, y + dy) for x, y in prevstep for dx, dy in diff])
    cleannextstep = nextstep & tablecells - passed
    prevstep = cleannextstep
    passed |= cleannextstep
    steps.append(prevstep)

print('steps:', len(steps) - 1)

concrete_steps = [B]
for i in range(len(steps) - 2, -1, -1):
    currentstep = concrete_steps[-1]
    posible = [(currentstep[0] + dx, currentstep[1] + dy) for dx, dy in diff]
    prevstep = [p for p in steps[i] if p in posible][0]
    concrete_steps.append(prevstep)

print(' '.join(map(index2point, reversed(concrete_steps))))
