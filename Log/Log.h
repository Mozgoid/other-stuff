#pragma once

#include <iostream>
#include <vector>
#include "StringHash/string_id.h"
#include <memory>
#include <map>
#include <assert.h>
#include <sstream>

using String = std::string;

template< typename T>
using Vector = std::vector<T>;

template< typename K, typename V>
using MultiMap = std::multimap<K, V>;

template< typename T>
using ShPtr = std::shared_ptr<T>;

using std::make_pair;

#define Assert assert;

namespace Log
{

    static const char* DefaultLogName = "default";

    enum Level
    {
        Debug,
        Info,
        Warning,
        Error,
        Disabled
    };

    class Logger
    {
    public:
        Logger(StringId name) : _name(name), _level(Info) {}
        virtual ~Logger() {}

        virtual void log(const String& message) = 0;

        StringId    name() const { return _name; }
        Level       level() const { return _level; }
        void        setLevel(Level level) { _level = level; }
    private:
        StringId _name;
        Level _level;
    };


    class ConsoleLogger : public Logger
    {
    public:
        ConsoleLogger(StringId name) : Logger(name) {}

        virtual void log(const String& message) override
        {
            std::cout << name().getStr() << ": " << message << std::endl;
        }
    };



    class LogManager final
    {
    public:
        void addLogger(ShPtr<Logger> logger);
        void log(StringId name, Level level, const String& message);
    private:
        MultiMap<String, ShPtr<Logger>> _loggers;
    };



    //-----------------------------------------------------------------------------
    // Purpose: this function called at end of writing values in the stream when no more values left 
    // function void writeValuesToStream(std::stringstream& stream, T nextValue, Args... args) 
    // can't be called because it has to many params
    // purpose of this is to stop recursion and finally return string with the message that we need
    //-----------------------------------------------------------------------------
    template<typename... Args>
    void writeValuesToStream(std::stringstream& stream, Args... args)
    {
        //do nothing, every value already in stream
    }


    //-----------------------------------------------------------------------------
    // Purpose: recursively calls itself and on every call put next value in stream
    // if args not empty then on next call first argument of them will become T nextValue
    // if args are empty then on next call other function -
    // void writeValuesToStream(std::stringstream& stream, Args... args)
    // will be called
    //-----------------------------------------------------------------------------
    template<typename T, typename... Args>
    void writeValuesToStream(std::stringstream& stream, T nextValue, Args... args)
    {
        stream << nextValue;
        writeValuesToStream(stream, args...);
    }



    //-----------------------------------------------------------------------------
    // Purpose: create a string by concatenation of arguments
    //-----------------------------------------------------------------------------
    template<typename... Args>
    String createMessage(Args... args)
    {
        std::stringstream stream;
        writeValuesToStream(stream, args...);
        return stream.str();
    }


    //-----------------------------------------------------------------------------
    // Purpose: return pointer to global log manager,
    // create manager if it's not there and add default loggers
    //-----------------------------------------------------------------------------
    LogManager* instance();


    //-----------------------------------------------------------------------------
    // Purpose: destroy global instance of log manager
    //-----------------------------------------------------------------------------
    void destroyInstance();



    //-----------------------------------------------------------------------------
    // Purpose: short version of log message calls
    //-----------------------------------------------------------------------------
    void debug(const String& message, StringId name = StringId(DefaultLogName));
    void info(const String& message, StringId name = StringId(DefaultLogName));
    void warning(const String& message, StringId name = StringId(DefaultLogName));
    void error(const String& message, StringId name = StringId(DefaultLogName));


    //-----------------------------------------------------------------------------
    // Purpose: add loggers that use standard output and default log file
    //-----------------------------------------------------------------------------
    void addStandardLoggers(StringId name);


}