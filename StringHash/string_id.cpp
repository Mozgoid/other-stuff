#include "string_id.h"

std::unordered_map<StringId::Hash, StringId::StringInfo> StringId::s_stringTable;


StringId::StringId(const std::string& str)
: Base(str)
{
    create(str.c_str());
}


StringId::StringId(const StringId& copy)
: Base(copy)
{
    ++s_stringTable[hash()].count;
}


StringId::StringId(const char* str)
: Base(str)
{
    create(str);
}


StringId::~StringId()
{
    decreaseCounter();
}


StringId& StringId::operator=(const StringId& copy)
{
    if (this == &copy)
        return *this;

    decreaseCounter();
    Base::operator=(copy);

    ++s_stringTable[hash()].count;

    return *this;
}


void StringId::decreaseCounter()
{
    const auto h = hash();
    assert(s_stringTable[h].count > 0);

    if (--s_stringTable[h].count == 0)
        s_stringTable.erase(h);
}


void StringId::create(const char* str)
{
    const auto h = hash();
    const char* validStr = str != nullptr ? str : "";

    auto it = s_stringTable.find(h);
    if (it == end(s_stringTable))
    {
        auto info = StringInfo{ validStr, 1 };
        s_stringTable.insert(std::make_pair(h, info));
    }
    else
    {
        assert(it->second.str == validStr);
        ++(it->second.count);
    }
}
