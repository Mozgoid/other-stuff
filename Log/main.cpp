// SuperGame.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "src/Log.h"

int _tmain(int argc, _TCHAR* argv[])
{
    Log::info("game was started");
    auto name = StringId("cool logger");
    Log::addStandardLoggers(name);
    Log::error("oh my god!", name);
    Log::addStandardLoggers(name);
    Log::warning("echo!", name);
    Log::warning("echo!", StringId("cool blogger"));
    Log::destroyInstance();

    int a;
    std::cin >> a;

	return 0;
}

